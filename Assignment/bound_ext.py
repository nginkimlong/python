seq = [1, 2, 3, 4]
a, b, c, *d = seq
print(a, b, c, d)
a, b, c, d, *e = seq
print(a, b, c, d, e)
a, *b, c, *d = seq
