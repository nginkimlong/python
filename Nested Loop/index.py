lst1 = [1, 2, 3, 4, 7, 10, 12, 40, 65, 43]
lst2 = [3, 4, 1, 2]
# for i in lst2: 
#     for j in lst1: 
#         if j == i: 
#             print("Founded ", j)
#         else: 
#             print("Not found : ", j)

print("==================")
for i in lst2: 
    if i in lst1: 
        print("Found ", i)
    else: 
        print("Not found")


lst_student = [x * 3 for x in range(1, 100)]
if lst_student: 
    print("Hello world")
else: 
    print("No data")
