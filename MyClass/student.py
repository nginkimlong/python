
class StudentClass:
    
    def __init__(self, full_name, age, email, tel):
        self.full_name = full_name
        self.age = age
        self.email = email 
        self.tel = tel
        self.room = "Student Hall 001"

    def change_room(self, new_room): 
        self.room = new_room

    def showStudent(self): 
        print("Student name is: ", self.full_name, " Age :", 
        self.age, " Email: ", self.email, "Room: ", self.room)
    

obj = StudentClass("Bong Seyha", "21", "seyha.com", "012888888")
# print("Student : ", obj.tel) 
obj.showStudent()  

# Change by instance 
obj.room = "Student hall 002"
obj.showStudent() 

# Change by method
obj.change_room("Student hall 003")
obj.showStudent()