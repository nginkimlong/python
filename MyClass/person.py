class Person:

    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def addTwoValue(self):
        return 1 + 2

obj = Person("Virak", "Soem")
print("His name is: ", obj.first_name, " - ", obj.last_name)

print(obj)
print(obj.addTwoValue())