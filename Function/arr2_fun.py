def make_pizza(name, price, **descirption):
    pizza = {} 
    pizza['pizza_name'] = name 
    pizza['pizza_price'] = price
    for k, v in descirption.items(): 
        pizza[k] = v
    return pizza

result = make_pizza("Sea Food Tropic", 23, category1="see food", size1="midium", sok="Sen Sok")
print("Result is: ", result)