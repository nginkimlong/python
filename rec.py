def showTriangle(n): 
    m = 1
    for i in range(0, n): 
        print("*" * i)
showTriangle(10)

print("\n")
def showReverseTriangle(n): 
    for i in range(0, n):  
        n = n -1
        print("*" * n)

showReverseTriangle(10)