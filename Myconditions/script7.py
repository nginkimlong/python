x = 10 
match x: 
    case 5: 
        return "passed"
    case 6: 
        return "E"
    case 7:
        return "D"
    case 8: 
        return "C"
    case 9:
        return "B"
    case _:
        return "A"

if x == 5: 
    print()
elif x == 6: 
    print() 
