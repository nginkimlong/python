# x = []
# for i in range(0, 90): 
#     x.append(i)
# print(x)

x = {"i"+str(i): i for i in range(0, 10)}
print(x)
print(type(x))

x = {}
for i in range(0,10):
    x["i"+str(i)] = i

for key, value in x.items(): 
    print(key, value)

print(sorted(x, reverse=True))