items = ["aaa", 111, (4, 5), 2.01] 
tests = [(4, 5), 3.14]
for key in tests: # For all keys
    for item in items: # For all items
        if item == key: # Check for match
            print(key,"was found")
            break
        else:
            print(key, "not found!")