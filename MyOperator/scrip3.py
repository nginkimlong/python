import math
s1 = input("Score 1: ")
s2 = input("Score 2: ")
s3 = input("Score 3: ")
avg = (int(s1) + int(s2) + int(s3)) / 3

print("Your average score is: ", math.floor(avg))
if avg >= 50: 
    print("Passed")
else: 
    print("Failed")