class Room: 
    def __init__(self, id, name, size):
        self.id = id
        self.name = name 
        self.size = size 
    def show_room_detail(self): 
        print("Room ID: ", self.id, " Room name: ", self.name, " Room Size: ", self.size)

    # def show_even_odd(self):
    #     if self.size % 2 == 0: 
    #         print("Even student")
    #     else: 
    #         print("Odd student")

class Campus: 
    def __init__(self, campus):
        self.room = Room("004", "Prasat Angkor", 1900)
        self.campus = campus
    def show_campus(self):
        print(self.campus)

# class Student(Room):
#     def __init__(self, id, name, size, arr):
#         super().__init__(id, name, size) 
#         self.arr = arr
#     def show_student_room(self): 
#         for i in self.arr: 
#             print(i)
#     def show_even_odd(self):
#         if self.size % 2 == 0: 
#             print("Even student: ", self.size)
#         else: 
#             print("Odd student: ", self.size)

# obj = Room("001", " Prasat Lolei", 99) 
# obj.show_room_detail() 
# obj.show_even_odd()

# print("==================")

# arr = ["Student A", "Student b", "Student C"]

# obj1 = Student("002", " Prasat Kob", 100, arr)
# obj1.show_room_detail()
# obj1.show_student_room()
# obj1.show_even_odd()

# print("===================")
# obj2 = Campus("New Campus")
# obj2.room.show_room_detail()
# obj2.room.show_even_odd()
# obj2.show_campus()