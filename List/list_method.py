students = [1, 2, 3]
# append number to The last index 
students.append(4) 
print(students)
# Copy all item from students list to student2 list
student2 = students.copy()
print("List of student Two is:", student2)
# Count items from a list 
my_count = ['to', 'be', 'or', 'not', 'to',  'be'].count('to')
print(my_count)
# Find index of list
knights = ['We', 'are', 'the', 'knights', 'who', 'say', 'ni']
print("Index of Knights is: ", knights.index("knights"))
students.extend(knights)
print(students)
students.insert(1, "New Object")
print(students)
students.pop(1)
print(students)
knights.sort()
print(knights)
print("==============================")
n_student = ["Hello", "Welcome", "be", "Good", "be", "human"]
n_student.reverse()
print("Reverse: ", n_student)
n_student.remove("be")
print("After removing: ", n_student)
n_student.sort()
print("After sorting: ", n_student)
n_student.sort(reverse=True)
print("After reversing sort", n_student)





