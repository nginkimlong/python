class Car: 
    def __init__(self, make, model, year):
        self.make = make 
        self.model = model
        self.year = year 
        self.odometer_reading = 0
    def get_descriptive_name(self):
        full_name = str(self.year) + ' ' + self.make + ' ' + self.model
        return full_name.title()
    def read_odometer(self):
        print("This car has " + str(self.odometer_reading) + " miles on it.")

    def update_odometer(self, mileage):
        if mileage >= self.odometer_reading:
            self.odometer_reading = mileage
        else:
            print("You can't roll back an odometer!")
    def increment_odometer(self, miles):
        self.odometer_reading += miles

class ElectictCar(Car): 
    def __init__(self, make, model, year):
        super().__init__(make, model, year)
        self.battery_size = 10 
    def show_battery(self): 
        print(self.make, " car model: ", self.model, 
        " Has battery: ", self.battery_size)
obj = ElectictCar("Tesla", "SUV", 2021)
print(obj.get_descriptive_name())
obj.show_battery()
