class Dog: 
    # Constructor of Dog class 
    def __init__(self, name, age):
        self.name = name
        self.age = age
    # Method sit 
    def sit(self):
        print(self.name.title() + " is now sitting.")
    # Method roll_over
    def roll_over(self):
        print(self.name.title() + " rolled over!")
obj = Dog("Kiki", 12)
print("This dog name is: ", obj.name, " Age: ", obj.age)
obj.sit()
obj.roll_over()