is_activated = False 
print("Default value is: ", is_activated)

if(is_activated == False):
    print("Wrong")
    is_activated = True
    print("Change value: ", is_activated)
else:
    print("Correct")
    is_activated = False
    print("Change value: ", is_activated)
num1 = 100
num2 = 120
print("Comparison two values: ", num1 < num2)