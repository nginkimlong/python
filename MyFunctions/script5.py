def showStudent(lst_student): 
    if lst_student: 
        for s in lst_student: 
            print("His name is: ", str(s).title())
    else: 
        print("No student")

lst_student = ["Sok", "Sav", "Chan", "Sun", "April"]
showStudent(lst_student)
