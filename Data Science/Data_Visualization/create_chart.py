import numpy as np 
import matplotlib.pyplot as plt
x = np.arange(0,10)
y = x ^ 2 
# print(x) 
# print(y)
plt.title("Title label")
plt.xlabel("X label")
plt.ylabel("Y label")
plt.plot(x, y, 'b')
plt.plot(x, y, '<')
plt.show()
# plt.savefig('my_file.pdf', format='pdf')


