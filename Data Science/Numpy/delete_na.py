import pandas as pd 
import numpy as np 

number = np.random.randn(5, 5)
data = pd.DataFrame(number, index=[1, 2, 3, 4, 5], columns=[1, 2, 3, 4, 5])
data = data.reindex(['a', 'b', 'c', 'd', 'e'])
drop_na_data = data.dropna()
print(drop_na_data)
