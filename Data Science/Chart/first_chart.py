from sqlalchemy import create_engine
import numpy as np 
import matplotlib.pyplot as plt 
import pandas as pd 
# scipy 


conn = create_engine('mysql+mysqldb://root:619916123@127.0.0.1:3306/db_datascience_test') 
data = pd.read_sql_query("SELECT id, writter, price FROM tbl_staff_buying ORDER BY price ASC", conn)
pf = pd.DataFrame(data)
x = []
y = [] 
for i in pf.index: 
    price = pf['price'][i]
    id = pf['id'][i]
    x.append(id)
    y.append(price)

plt.plot(x, y)
plt.xlabel("ID")
plt.ylabel("Price")
plt.title("Selling Graphic")
plt.legend(["ID 01", "ID 02", "ID 03", "ID 04"], loc= 4)
plt.show()