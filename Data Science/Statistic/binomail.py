from turtle import color
from numpy import size
from scipy.stats import binom, norm
import seaborn as sb
import matplotlib.pyplot as plt

binom.rvs(size=4,n=20,p=0.8) #Random variates of given size
# binom.rvs(size=(0, 10), n=20, p=1)
data_binom = binom.rvs(n=20,p=0.8,loc=0,size=1000)
ax = sb.displot(data_binom, kde=True, color="y")
# ax.set(xlabel='Binomial', ylabel='Frequency')
plt.xlabel("Binomial")
plt.ylabel("Frequency")
plt.show()

