from itertools import count
import pandas as pd
import matplotlib.pyplot as plt 
import numpy as np

# api_data = pd.read_json("https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=a08d77da7eed461cb351e731142e6bdf")
# print("API DATA: ")
# print(api_data)

print("*******************************")
data = pd.read_excel("/Users/kimlongngin/Desktop/Python-Class/Data Science/Database/combine_biopesticide survey 2021.xlsx")
# r = data[0:10]['Q6_age_year', 'Q11_income_dollar']
df = pd.DataFrame(data)
my_data= []
for i in range(0, len(df)):
       my_data.append(i)

r = data.loc[my_data,['Q6_age_year', 'Q11_income_dollar']]

r_age = data.loc[my_data,['Q6_age_year']]
df_age = pd.DataFrame(r_age)


df = pd.DataFrame(r)

# 1. Series, DataFrame
#Create a Dictionary of series

print("Mean Values in the Distribution")
print(df.mean())

print("*******************************")
print("Median Values in the Distribution")
print(df.median())

# Define mode
print("*******************************")
print("Mode Values in the Distribution")
mode_value = df.mode()
print(mode_value)

# Define mode
print("*******************************")
print("Deviation Values in the Distribution")
deviation_value = df.std()
print(deviation_value)

# Define mode
print("*******************************")
print("Expo Values in the Distribution")
skew_value = df.skew()
print(skew_value)


# mu, sigma = 0.5, 0.1
# Create the bins and histogram
# count, bins, ignored = plt.hist(df_age, 20, density=True, stacked=True)


# Plot the distribution curve
# plt.plot(bins, 1/(sigma * np.sqrt(2 * np.pi)) * \
#     np.exp( - (bins - mu)**2 / (2 * sigma**2) ), linewidth=3, color='y')
# plt.xlabel("Variance")
# plt.ylabel("Normal number")
# plt.show()