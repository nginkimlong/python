from sqlalchemy import create_engine
from pandas.io import sql
import pandas as pd 
import pymysql

# ==================== FIRST STYLE ==============
con = create_engine('mysql+mysqldb://root:619916123@127.0.0.1:3306/db_datascience_test')
sql.execute("INSERT INTO `tbl_employee2` (`index`, `id`, `name`, `salary`, `start_date`, `dept`) \
    VALUES (%s, %s, %s, %s, %s, %s)", con, params=[(12, 11,'TESTING NEW FIELD',1200,'2022-05-01','DIT')])

data = pd.read_sql_query("SELECT * FROM tbl_employee2", con)
# print(data)

# ========================== SECOND Style ==============================
# connection = pymysql.connect(host='127.0.0.1',
#                              user='root',
#                              password='619916123',
#                              db='db_datascience_test')
# cursor.execute("SELECT * FROM tbl_employee2")
# row = cursor.fetchall()
# for i in row: 
#     print(i[2])
# cursor = connection.cursor()
# sql = "INSERT INTO `tbl_employee2` (`index`, `id`, `name`, `salary`, `start_date`, `dept`) VALUES (%s, %s, %s, %s, %s, %s)"
# cursor.execute(sql, (9, 8,'NGINKIMLONG',1200.45,'2021-10-01','IT'))
# connection.commit()

