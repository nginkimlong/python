import pandas as pd 
data = pd.read_csv("/Users/kimlongngin/Desktop/Python-Class/Data Science/CSV/data.csv")

# Read data by specific row and column 
new_data = data.loc[0:2, ['name', 'income']]
print(new_data)
total = 0 
pd_data = pd.DataFrame(new_data)
for i in pd_data.index: 
    income = pd_data['income'][i]
    total = total + income
print("Total income is: ", total)

#  Read Specific column
# s_data_name = data[0:2]['name']
# s_data_income = data[0:2]['income']
# print(s_data_name, s_data_income)

# Filter data from loop
# pd_data = pd.DataFrame(data)
# for i in pd_data.index:
#     name = pd_data['name'][i]
#     income = pd_data['income'][i]
#     print("Name : ", name, " Income is: ", income)
